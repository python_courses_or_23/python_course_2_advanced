def parse_ranges(ranges_string):
    """
    This function receives a string in the format of
    startRange1-endRange1, startRange2-endRange2, startRange3-endRange3 ...
    and returns a generator of numbers that are inside the ranges
    :param ranges_string: the string of ranges
    :type ranges_string: string
    :return: a generator of the numbers
    :rtype: generator
    """
    # this generator will generate the ranges from the given string in the format of (start, end)
    ranges_generator = ((int(r.split("-")[0]), int(r.split("-")[1])) for r in ranges_string.split(","))

    # this generator will generate the numbers in the first-generator ranges
    numbers_generator = (i for (start, end) in ranges_generator for i in range (start, end +1) )

    return numbers_generator

print(list(parse_ranges("1-2,4-4,8-10")))
print(list(parse_ranges("0-0,4-8,20-21,43-45")))
