def translate(sentence):
    """
    This function uses a dict of words from spanish to english to translate a given string
    "sentence" in spanish and returns its appropriate translate to the english langugue
    :param sentence: the given sentence
    :type sentence: string
    :return: its translates english version
    :rtype: string
    """
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    splits = sentence.split()
    print(splits)
    ret_str = " ".join(words[word] for word in splits)
    return ret_str

print(translate("el gato esta en la casa"))
