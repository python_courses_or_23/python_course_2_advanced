def gen_secs():
    """
    This function creates and returns a generator of seconds
    i.e. a generator of all numbers from 0 to 59
    :return: the seconds generator
    :rtype: generator
    """
    return (sec for sec in range (60))
def gen_minutes():
    """
    This function creates and returns a generator of minutes
    i.e. a generator of all numbers from 0 to 59
    :return: the minutes generator
    :rtype: generator
    """
    return (min for min in range (60))
def gen_hours():
    """
    This function creates and returns a generator of hours
    i.e. a generator of all numbers from 0 to 23
    :return: the hours generator
    :rtype: generator
    """
    return (hour for hour in range(24))
def gen_time():
    """
    This function creates and returns a string representing the time,
    which consists of the seconds, minutes and hours. it does so by generating a
    number for the seconds each iteration and checking whether the minutes generator need
    to generate another minutes, if so, it performs a simillar check for the hours
    :return: the seconds generator
    :rtype: string
    """
    sec_gen = gen_secs()
    min_gen = gen_minutes()
    hour_gen = gen_hours()
    sec = next(sec_gen)
    min = next(min_gen)
    hour = next(hour_gen)

    while True:
        str = "%02d:%02d:%02d" %(hour, min, sec)
        yield str

        if sec == 59:
            sec_gen = gen_secs()
            if min == 59:
                min_gen = gen_minutes()
                if hour == 23:
                    hour_gen = gen_hours()
                hour = next(hour_gen)
            min = next(min_gen)
        sec = next(sec_gen)

def gen_days(month, leap_year=True):
    """
    This function creates and returns a generator of days in a given month
    :param month: the given month
    :type month: int
    :param leap_year: a parameter representing if it's a leap year or not,
           critical for febuary days (29 instead of 28)
    :type leap_year: boolean / bool
    :return: a generator of the days in the given month
    :rtype: generator
    """
    if month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12: # 31 days
        return (day for day in range (1,32))
    elif month == 4 or month == 6 or month == 9 or month == 11: # 30 days
        return (day for day in range(1, 31))
    elif leap_year:
        return (day for day in range(1, 30)) # 29
    return (day for day in range(1, 29))
def gen_months():
    """
    This function creates and returns a generator of months
    i.e. a generator of all numbers from 1 to 12
    :return: the months generator
    :rtype: generator
    """
    return (month for month in range (1,13))
def gen_years(start=2023):
    """
    This function creates and returns a generator of years
    i.e. a generator of all numbers from the starting point which is
    automatically 2023, and on
    :param start: the starting year (2023 by default)
    :type start: int
    :return: the years generator
    :rtype: generator
    """
    temp = start
    while True:
        yield temp
        temp += 1

def isLeapYear(year):
    """
    This function returns whether a given year is a leap year i.e. is it :
    1) divisible by 4 and not 100 --> leap year
    2) divisible by 4 and 100 and 400 --> leap year
    :param year: the given year
    :type year: int
    :return: whether a given year is a leap year
    :rtype: boolean / bool
    """
    if year % 4 ==0 and year % 100 != 0: # divisible by 4 and not 100
        return True
    return year % 4 ==0 and year % 100 ==0 and year % 400 ==0


def gen_date():
    """
    This function creates and returns a string representing the full time and date,
    which consists of the day, month, year and time (seconds, minutes and hours)
    it does so by generating the "next time" in every iteration and checking whether the days generator need
    to generate another day, if so, it performs a simillar check for the month, and for the year
    :return: the "full time and date" string
    :rtype: string
    """
    time_gen = gen_time()
    time = next(time_gen)

    month_gen = gen_months()
    month = next(month_gen)

    year_gen = gen_years()
    year = next(year_gen)

    day_gen = gen_days(month, isLeapYear(year))
    day = next(day_gen)

    while True:
        yield "%02d/%02d/%04d %s" % (day, month, year, time)
        if time == "23:59:59":
            time_gen = gen_time()
            # checking if this is the last day of the month
            try:
                day = next(day_gen)
            except StopIteration : # this was the last day of that month
                if month == 12:
                    month_gen = gen_months()
                    year = next(year_gen)
                month = next(month_gen)
                # checking if this is the last month of the year
                day_gen = gen_days(month, isLeapYear(year))
        time = next(time_gen)


def main():
    for gt in gen_date():
        print(gt)

if __name__ == "__main__":
    main()