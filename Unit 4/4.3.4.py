def get_fibo():
    """
    This is a generator function which yields the fibonacci sequence numbers
    e.g. 0,1,1,2,3,5,8,13,21 ...
    according to the formula of : fib(n) = fib(n-1) + fib(n-2),
    where fib(0) = 0 and fib(1) = 1
    """
    prev = 0
    current = 1
    yield 0
    while True:
        yield current
        (current, prev) = (current+prev, current)

fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))

