def first_prime_over(n):
    """
    This function returns the first prime number that is greater than the given number "n"
    :param n: the given number
    :type n: int
    :return: the first prime number that is greater than the given number "n"
    :rtype: int
    """
    number_gen = (i for i in range(n+1,n*2) if is_prime(i))
    return next(number_gen)

def is_prime(n):
    """
    This function returns whether a given number "n" is a prime number
    :param n: the given number
    :type n: int
    :return: whether the number is a prime
    :rtype: boolean / bool
    """
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

print(first_prime_over(5))
