import winsound

freqs = {"la": 220,
         "si": 247,
         "do": 261,
         "re": 293,
         "mi": 329,
         "fa": 349,
         "sol": 392}
notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"
# note,duration-note,duration
notes_duration_splits = notes.split("-")
# now we have ["note,duation", "note_duration",...], the returned object is INDEED an iterable due to it being a list
# winsound.Beep(frequency, duration) in order to make a sound using the winsound module
notes_iter = iter(notes_duration_splits)
while True :
    try:
        note_tuple = next(notes_iter) # generate the next (note, duration) tuple
        note_duration = note_tuple.split(",") # split into note & duration pair
        winsound.Beep(freqs[note_duration[0]], int(note_duration[1])) # beep (make a sound) according to the dict value of the note i.e. the frequency, and its diuration
    except StopIteration:
        break
