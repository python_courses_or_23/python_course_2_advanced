numbers = iter(list(range(1, 101))) # a iterator of the list of  numbers from 1 to 100
for i in numbers: # running with the iterator
    try : # trying to get the next number from the iterator
        i = next(numbers)
        try: # trying to get the next number from the iterator
            i = next(numbers)
            print(i) # printing every THIRD number
        except:
            break
    except:
        break
