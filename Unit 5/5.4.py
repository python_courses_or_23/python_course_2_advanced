import functools
# id is a number of 9 digits with certain limitaions
"""
Integrity checking of an id number :
    1) multiplying each digit by 1/2 accroding to its position in the number :
       1 if odd and 2 if even, starting from the position of 1 (not 0)
    2) for each new number in the id - if bigger than 9, sum its digits
    3) sum all numbers yet again
    4) if the sum is divisible by 10 - OK (valid id number), otherwise NOT (not a valid id number)
"""

class IDiterator :
    """
    A class to represent an IDiterator object
    This IDiterator class implements the iterator protocol i.e. implements the iter and next methods
    and raises the "StopIteration" error when needed
    ----------
    Attributes
    ----------
    last_id - the last id the was validity-checked
    id - the starting id
    -------
    Methods
    -------
    1) __iter__ - this is the iterator instance method which returns the object itelf, since it implements the needed protocol
    2) __next__ - this instance method returns the next element of the iterator, it does so following these few steps:
                  1) it increments the note index
                  2) it checks if the octave index has gone over the maximum amnout
                     and if so, it raises the StopIteration error
                  3) it sets the value of the returning note by the formula of frequencyList[note index] * 2^(octave index)
                  2) Otherwise, it checks if the current note index in the last one in the octave,
                     if so, it increments the octave index and sets the note index to -1
    """
    def __init__(self, id):
        """
        This method constructs all the necessary attributes
        for the IDiterator execption object i.e. the last id and the strating id
        :param _id: the starting id
        :type _id: int
        :rtype: None
        """
        self.last_id = id
        self._id = id
    def __iter__(self): # the iterator method
        return self
    def __next__(self): # the next method for the iterator protocol
        self.last_id += 1
        if check_id_valid(self.last_id) and not self.last_id == self._id:
            return self.last_id -1
        while not check_id_valid(self.last_id): # invalid id
            if self.last_id >= 999999999:
                raise StopIteration
            self.last_id += 1
        return self.last_id
def id_gen (id_num):
    """
    This is a generator function which yields the next id number, starting from the id number given,
    not including it. it increments the id number firstly, then porforms the validity check on it and increments it
    until reaching a valid id number, then yielding it.
    :param id_num: the starting id number
    :type id_num: int
    :rtype: yielding an id number, integer (int), raises a StopIteration error at the end of the while loop
    """
    last_id = id_num
    while last_id < 999999999:
        last_id += 1
        while not check_id_valid(last_id):
            last_id += 1
        yield  last_id
    raise StopIteration

def mult_step_1 (i, string):
    """
    This is a function for the first step out of 4 in the integrity check for the id number.
    it recieves a string representing the id number and an index i in it, to perform the following steps :
    if i is even ==> return the number, else, return the number, doubled.
    Please notice i had filpped the statement from the one described in step 1 in the intro (at the top of the file) since the index are starting from zero instead of one
    :param i: the index
    :type i: int
    :param string: the string of the id
    :type string: string
    :return: the new number
    :rtype: int
    """
    if i%2 ==1:
        return int(string[i]) *2
    return int(string[i])
def reducing_9 (number):
    """
    This is a function for the second step out of 4 in the integrity check for the id number.
    it recieves a number representing each "new number" of the id, after step 1 had occured,
    if the number is greater than 9, it returns the sum of its digits, and number othwerwise
    :param number: the number
    :type number: int
    :return: the new number (sum of digits / same number)
    :rtype: int
    """
    if number > 9:
        return number%10 + int(number/10)
    return number

def check_id_valid (id_number): # id number is a number
    """
    This is the whole integrity check function i.e. all four steps described earlier,
    it returns whether that id number is valid
    :param id_number: the given id number
    :type id_number: int
    :return: whether that id number is legal / valid
    :rtype: boolean / bool
    """
    id_str = str(id_number) # converting the number to a string
    step_1 = [mult_step_1(i, id_str) for i in range(len(id_str))] # preforming step 1 to get a new list representing the new id numbers (STEP 1)
    step_2 = [reducing_9(num) for num in step_1] # for each new number that was produced after step 1, we change it if needed using the reducing_9 function (STEP 2)
    return functools.reduce(lambda x,y : x+y, step_2) % 10 == 0
    # reducing the previous list using the reduce function from the functools moudle
    # to recive the final sum, and checking if the sum is divisible by 10 (STEP 3 & 4)

def main():
    id_num = int(input("Please enter a 9 digit number representing an ID : "))
    it_or_gen = input("Generator or Iterator? (gen/it)? ") # generator or iterator (identical outcome)
    if it_or_gen == "it" :
        iter_id = iter(IDiterator(id_num))
        for i in range (10):
            print(next(iter_id))
    elif it_or_gen == "gen":
        gen_id = id_gen(int(id_num))
        for i in range (10):
            print(next(gen_id))

if __name__ == "__main__":
    main()