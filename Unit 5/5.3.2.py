class MusicNotes :
    """
    A class to represent a MusicNotes object
    This MusicNotes class implements the iterator protocol i.e. implements the iter and next methods
    and raises the "StopIteration" error when needed
    ----------
    Attributes
    ----------
    _notes_freqs - the list of seven frequencies i.e. (do, re, mi, fa, sol, la, si)
    _note_indx - a note index representing which note was chosen last (0 to 6)
    _octav_indx - an octave index representing which octave is the current one
    _octav_num - the total number of octaves
    -------
    Methods
    -------
    1) __iter__ - this is the iterator instance method which returns the object itelf, since it implements the needed protocol
    2) __next__ - this instance method returns the next element of the iterator, it does so following these few steps:
                  1) it increments the note index
                  2) it checks if the octave index has gone over the maximum amnout
                     and if so, it raises the StopIteration error
                  3) it sets the value of the returning note by the formula of frequencyList[note index] * 2^(octave index)
                  2) Otherwise, it checks if the current note index in the last one in the octave,
                     if so, it increments the octave index and sets the note index to -1
    """
    def __init__(self):
        """
        This method constructs all the necessary attributes
        for the MusicNotes execption object i.e. the list of frequencies, the noteIndex, the octaveIndex and lastly the octaves quantity
        :rtype: None
        """
        self._notes_freqs = [55, 61.74, 65.41, 73.42, 82.41, 87.31, 98] # all seven notes (in first octave frequency form), which will be doubled through the octaves
        self._note_indx = -1
        self._octav_indx = 0
        self._octav_num = 5
    def __iter__(self): # the iterator method
        return self
    def __next__(self): # the next method
        self._note_indx += 1
        if self._octav_indx == self._octav_num : # after last octave
            raise StopIteration
        ret =  self._notes_freqs[self._note_indx] * (2**(self._octav_indx))
        if self._note_indx == len(self._notes_freqs) -1 : # last note in non-last octave
            self._octav_indx += 1
            self._note_indx = -1
        return ret

notes_iter = iter(MusicNotes())
for freq in notes_iter:
    print(freq)
