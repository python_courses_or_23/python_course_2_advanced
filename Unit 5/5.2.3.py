import itertools, functools
# 3 bills of 20, 5 bills of 10, 2 bills of 5 and 5 of 1
# what are all the ways to account for a sum of 100 ?
# as we saw eariler on the chapter, we can use the permutations or combinations functions
lst = [20] *3 + [10]*5 + [5]*2 + [1]*5
ret = list()
for i in range(7,len(lst)): # the combination could be of length 7 and above (7 beacuse of 3 of 20 and 4 of 10)
    for combin in itertools.combinations(lst, i): # for each combination of that length, of the lst
        if functools.reduce(lambda x, y : x+y, combin) == 100 and combin not in ret: # if the combination results in a sum of 100 and doesn't appear in the tuples list
            ret.append(combin) # add it to the returned list
for item in ret: # print all correct combinations
    print(item)


