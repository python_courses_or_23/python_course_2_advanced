def four_dividers (number):
    """
    This function recives a number and returns a list of all the numbers in the
    the range of 0 to the number (including the number) which are t amultiplication of 4
    :param number: the number
    :type number: int
    :return: the list of numbers from 1 to the number which are divisible by 4
    :rtype: list
    """
    return list(filter(is_four_mult, range(1,number)))

def is_four_mult (number) :
    """
    This function checks whether a number is divisible by 5
    :param number: a number to check
    :type number: int
    :return: whether that number is dividible by 4
    :rtype: bool / boolean
    """
    return number % 4 ==0

print(four_dividers(9))
print(four_dividers(3))
