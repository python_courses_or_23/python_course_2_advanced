def double_letter (string) :
    """
    This function double the letters of each string using the double_let function
    :param string: the string we would like its character doubled
    :type string: string
    :return: the double-characters string
    :rtype: string
    """
    return ''.join(list(map(double_let, string)))
def double_let(char) :
    """
    This function returns a double of a given string e.g. gg for g
    :param char: the given character (string)
    :type char: string
    :return: a string consisting of that char (string), twice
    :rtype: string
    """
    return char*2

print(double_letter("python"))
print(double_letter("we are the champions!"))