def is_funny(string):
    # we can add to the list all non a & h letters (chars) and then checking the returned list len
    """
    This function returns whether the given string is funny i.e. does it contain
    the chars 'a' and 'h' only.
    It does so by adding to a list all the char that are not 'a' / 'h', and
    checking if that list is empty at the end.
    :param string: the given string to check for
    :type string: string
    :return: whether that string is funny
    :rtype: bool / boolean
    """
    return len([char for char in string if char != 'h' and char != 'a']) == 0

print(is_funny("hahahahahaha"))
