import functools
"""
Section 1 - Write a program that prints to the screen the longest name in the file.
Here is the expected output: Valdimir. The soultion must include up to 2 lines of code

Section 2 - Write a program that prints to the screen the sum of the lengths of the names in the file.
Here is the expected output: 38. The soultion must include up to 4 lines of code

Section 3 - Write a program that prints to the screen the shortest names in the file, each name on a separate line.
Here is the expected output: 
Ed 
Jo
The soultion must include up to 4 lines of code

Section 4 - Write a program that creates a new file named name_length.txt that contains the length of each name in the names.txt file, in order, one per line. 
Here is the expected output:
4
4
8
7
2
5
6
2
The soultion must include up to 3 lines of code

Section 5, last part - Write a program that receives from the user a number representing the length of a name and prints all the names in the names.txt file that are of this length.
Below is a sample run and output:
Enter name length: 4
Hans
Anna
"""
# section 1
print(sorted(open ("names.txt", 'r').read().split('\n'), key=len, reverse=True)[0])
# section 2
print(functools.reduce(lambda x,y : x+len(y), open("names.txt", 'r').read().split('\n'),0))
# section 3
lst = (sorted(open("names.txt", 'r').read().split('\n'), key=len))
print('\n'.join([str for str in lst if len(str) == len(lst[0])]))

# section 4
lst = list(map(lambda string : str(len(string)),open("names.txt", 'r').read().split('\n')))
open("name_length.txt", 'w').write('\n'.join(lst))

#section 5
num = input("Enter name length : ")
lst = [i for i in range(len(open("name_length.txt", 'r').read().split('\n'))) if open("name_length.txt", 'r').read().split('\n')[i] == num]
print('\n'.join([open ("names.txt", 'r').read().split('\n')[x] for x in lst]))