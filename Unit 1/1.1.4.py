import functools

def sum_of_digits (number):
    """
    This function returns the sum of digis of a given number
    using the reduce function from the functools module / class,
    with a 0 initializer and uses the add_char function in the reducing
    :param number: the number
    :type number: int
    :return: the sum of digits
    :rtype: int
    """
    return functools.reduce(add_char, str(number),0)

def add_char (sum, num):
    """
    This function, used by the reduce function for the summing of a number as a char
    as well as the current sum
    :param sum: the current sum of numbers
    :type sum: int
    :param num: the new number, as a char (string)
    :type num: string
    :return: the new sum
    :rtype: int
    """
    return sum + int(num)

print(sum_of_digits(104))


