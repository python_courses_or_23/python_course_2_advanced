def intersection (list1, list2):
    # since the list formation created contains more than one of the same number, we can
    # use the set casting in order to cast it into set, which conatians only unique values,
    # then, we convert it back into a list, and returning it
    """
    This function returns a list of the shared numbers between two given lists
    The function uses the "set" casting in order to filter all doubles, as sets contain only unique values
    :param list1: the first list of integer numbers
    :type list1: list
    :param list2: the second list of integer numbers
    :type list2: list
    :return: the list of shared numbers of the two given lists
    :rtype: list
    """
    return list(set([x for x in list1 for y in list2 if x == y]))

print(intersection([1, 2, 3, 4], [8, 3, 9]))
print(intersection([5, 5, 6, 6, 7, 7], [1, 5, 9, 5, 6]))