def is_prime(number):
    # if the list of numbers which which are can divide the number without a remainder is empty
    # the number is a prime onw, and a non-prime otherwise
    """
    This function returns whether a given number is a prime number
    it does so by creating a list of all the numbers from 2 to that number
    and checking whether any of them are dividers of the original number
    if so, that number i.e. the dividor, is added to a list.
    In the end, we check if that list contain any numbers, if so, the original number isn't a prime
    :param number: the number we wish to check for
    :type number: int
    :return: whether that number is a prime number
    :rtype: bool / boolean
    """
    return len([x for x in range(2,number) if number%x == 0]) == 0

print(is_prime(42))
print(is_prime(43))