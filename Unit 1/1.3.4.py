password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
"""
We go over the string char by char, and for each char, we replace its value 
to be the next-next char e.g. a will change to c (a->b->c) and so on
ONLY IF THE CHAR IS NOT A SPACE (' ') OR A COLON (':')
Then, we join the lists elemets to form the final string
"""
print(''.join([chr(ord(c)+2) if c !=' ' and c != ':' else c for c in password]))
