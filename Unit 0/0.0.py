
def combine_coins_long_method (symbol, numbers_lst) :
    """
    This function creates a string of the coins with their sybmol, seperated by commas
    :param symbol: the symbol for the coins e.g. $, ₪ or £
    :type symbol: string
    :param numbers_lst: the money numbers, amount of units
    :type numbers_lst: list
    :return: a string representing the coins combined with the given symbol, seperated by commas
    :rtype: string
    """
    ret = ""
    for number in numbers_lst :
        ret += str(number) + symbol + ', '
    return ret[:-2]

def combine_coins_short_method (symbol, numbers_lst) :
    """
    This function creates a string of the coins with their sybmol, seperated by commas
    BUT IT DOES SO IN ONE LINE, using the map function, converting it into a list and then joining the list to form a string
    :param symbol: the symbol for the coins e.g. $, ₪ or £
    :type symbol: string
    :param numbers_lst: the money numbers, amount of units
    :type numbers_lst: list
    :return: a string representing the coins combined with the given symbol, seperated by commas
    :rtype: string
    """
    return ', '.join(list(map(lambda n : str(n) + symbol, numbers_lst)))


print(combine_coins_short_method('$', range(5)))