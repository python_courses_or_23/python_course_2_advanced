def read_file(file_name):
    """
    This procedure demonstrates the concept of try, except, else and finally
    It prints "__CONTENT_START__", then tries to open the file to reading,
    if it fails it prints "The given file name does not exist", otherwise it prints
    the contents of the file, and FINALLY it prints "__CONTENT_END__"
    :param file_name: the name of the file to read from
    :rtype: None
    """
    print("__CONTENT_START__")
    try:
        f = open (file_name, 'r')
    except FileNotFoundError:
        print("The given file name does not exist")
    else:
        print(f.read())
    finally:
        print("__CONTENT_END__")

