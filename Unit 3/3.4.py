import string

class UsernameContainsIllegalCharacter (Exception):
    """
    A class to represent an UsernameContainsIllegalCharacter execption, it extends the general "Exception" class
    and overides its __init__ and __str__ methods
    -------
    Methods
    -------
    1) __str__ - this instance method prints the appropriate string for the error, in this case
    			"The given username (username here) has illegal character (illegal character here) at index (index of illegal character here)"
    """
    def __init__(self, username, char, index):
        """
        This method constructs all the necessary attributes
        for the UsernameContainsIllegalCharacter execption object i.e. the username, the char
        and its index
        :param username: the username
        :type username: string
        :param char: the illegal character
        :type char: string
        :param index: the  illegal character's index in the username
        :type index: int
        :rtype: None
        """
        self._username = username
        self._char = char
        self._index = index
    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The given username (username here) has illegal character (illegal character here) at index (index of illegal character here)"
        :return: the appropriate string
        """
        return 'The given username %s has illegal character "%s" at index %d' % (self._username, self._char, self._index)
class UsernameTooShort  (Exception):
    """
    A class to represent an UsernameTooShort execption, it extends the general "Exception" class
    and overides its __init__ and __str__ methods
    -------
    Methods
    -------
    1) __str__ - this instance method prints the appropriate string for the error, in this case
    			"The given username (username here) is of (username's length here) characters when the minimum amount is 3."
    """
    def __init__(self, username):
        """
        This method constructs all the necessary attributes
        for the UsernameTooShort execption object i.e. its username
        :param username: the username
        :type username: string
        :rtype: None
        """
        self._username = username
    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The given username (username here) is of (length of username here) characters when the minimum amount is 3."
        :return: the appropriate string
        """
        return "The given username %s is of %d characters when the minimum amount is 3." % (self._username, len(self._username))
class UsernameTooLong(Exception):
    """
    A class to represent a UsernameTooLong exception, it extends the general "Exception" class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The given username (username here) is of (length of username here) characters when the maximum amount is 16."
    """
    def init(self, username):
        """
        This method constructs all the necessary attributes
        for the UsernameTooLong exception object i.e. its username
        :param username: the username
        :type username: string
        :rtype: None
        """
        self._username = username
    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The given username (username here) is of (length of username here) characters when the maximum amount is 16."
        :return: the appropriate string
        """
        return "The given username %s is of %d characters when the maximum amount is 16." % (self._username, len(self._username))

class PasswordMissingCharacter(Exception):
    """
    A class to represent a PasswordMissingCharacter exception, it extends the general "Exception" class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The password (password here) is missing a character."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordMissingCharacter exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password
    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The password (password here) is missing a character."
        :return: the appropriate string
        """
        return "The password %s is missing a character." % self._password
class PasswordMissingCharacterUpper(PasswordMissingCharacter):
    """
    A class to represent a PasswordMissingCharacterUpper exception, it extends the PasswordMissingCharacter class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The password (password here) is missing a character (Uppercase)."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordMissingCharacterUpper exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password
    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The password (password here) is missing a character (Uppercase)."
        :return: the appropriate string
        """
        return super().__str__() + " (Uppercase)"
class PasswordMissingCharacterLower(PasswordMissingCharacter):
    """
    A class to represent a PasswordMissingCharacterLower exception, it extends the PasswordMissingCharacter class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The password (password here) is missing a character (Lowercase)."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordMissingCharacterLower exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password

    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The password (password here) is missing a character (Lowercase)."
        :return: the appropriate string
        """
        return super().__str__() + " (Lowercase)"
class PasswordMissingCharacterDigit(PasswordMissingCharacter):
    """
    A class to represent a PasswordMissingCharacterDigit exception, it extends the PasswordMissingCharacter class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The password (password here) is missing a character (Digit)."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordMissingCharacterDigit exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password

    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The password (password here) is missing a character (Digit)."
        :return: the appropriate string
        """
        return super().__str__() + " (Digit)"
class PasswordMissingCharacterSpecial(PasswordMissingCharacter):
    """
    A class to represent a PasswordMissingCharacterSpecial exception, it extends the PasswordMissingCharacter class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The password (password here) is missing a character (Special)."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordMissingCharacterSpecial exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password

    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The password (password here) is missing a character (Special)."
        :return: the appropriate string
        """
        return super().__str__() + " (Special)"

class PasswordTooShort(Exception):
    """
    A class to represent a PasswordTooShort exception, it extends the general "Exception" class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The given password (password here) contains (length of password here) characters when the minimum amount is 8."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordTooShort exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password

    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The given password (password here) contains (length of password here) characters when the minimum amount is 8."
        :return: the appropriate string
        """
        return "The given password %s contains %d characters when the minimum amount is 8." % (
        self._password, len(self._password))
class PasswordTooLong(Exception):
    """
    A class to represent a PasswordTooLong exception, it extends the general "Exception" class
    and overrides its init and str methods
    -------
    Methods
    -------
    1) str - this instance method prints the appropriate string for the error, in this case
    "The given password (password here) contains (length of password here) characters when the maximum amount is 40."
    """
    def init(self, password):
        """
        This method constructs all the necessary attributes
        for the PasswordTooLong exception object i.e. its password
        :param password: the password
        :type password: string
        :rtype: None
        """
        self._password = password

    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The given password (password here) contains (length of password here) characters when the maximum amount is 40."
        :return: the appropriate string
        """
        return "The given password %s contains %d characters when the maximum amount is 40." % (
        self._password, len(self._password))

def main():
    #username = input("Please enter a valid input for a username : ")
    #password = input("Please enter a valid input for a password : ")
    try :
        check_input("A_1", "4BCD3F6h1jk1mn0p")
    except UsernameContainsIllegalCharacter as e :
        print(e.__str__())
    except UsernameTooShort as e:
        print(e.__str__())
    except UsernameTooLong as e :
        print(e.__str__())
    except PasswordMissingCharacter as e:
        print(e.__str__())
    except PasswordTooShort as e :
        print(e.__str__())
    except PasswordTooLong as e:
        print(e.__str__())



def check_input(username, password):
    """
    This function checks the validity of the username and password and
    raises an apporopriate execption if needed from the 10 various execptions described before
    :param username: a username
    :type username: string
    :param password: the username's password
    :type password: string
    :rtype: None
    """
    if len([char for char in username if not char.isalpha() and not char.isdigit() and char != '_']) == 0:
        if len(username) >= 3 and len(username) <= 16:
            # valid user name
            if contains_upper(password) and contains_lower(password) and contains_digit(password) and contains_special(password):
                if len(password) >= 8 and len(password) <= 40:
                    print("OK")
                else:
                    if len(username) < 8:
                        raise PasswordTooShort(password)
                    else:
                        raise PasswordTooLong(password)
            else:
                if not contains_upper(password):
                    raise PasswordMissingCharacterUpper(password)
                elif not contains_lower(password):
                    raise PasswordMissingCharacterLower(password)
                elif not contains_digit(password):
                    raise PasswordMissingCharacterDigit(password)
                else :
                    raise PasswordMissingCharacterSpecial(password)
        else : # ilgeal charcters
            if len(username) < 3:
                raise UsernameTooShort(username)
            else:
                raise UsernameTooLong(username)
    else:
        lst = [char for char in username if not char.isalpha() and not char.isdigit() and char != '_']
        raise UsernameContainsIllegalCharacter(username, lst[0], username.index(lst[0]))

def contains_upper(string):
    """
    This function checks if a string contains an upper letter
    :param string: the checked string
    :return: whether that string contains an upper letter
    """
    for char in string:
        if ord(char) <= 90 and ord(char) >= 65 :
             return True
    return False
def contains_lower(string):
    """
    This function checks if a string contains a lower letter
    :param string: the checked string
    :return: whether that string contains a lower letter
    """
    for char in string:
        if ord(char) <= 122 and ord(char) >= 97 :
            return True
    return False
def contains_digit(string):
    """
    This function checks if a string contains a digit
    :param string: the checked string
    :return: whether that string contains a digit inside it
    """
    for char in string:
        if char.isdigit():
            return True
    return False
def contains_special(str):
    """
    This function checks if a string contains one or more of the special characters
    descirbed in the "string.punctuation" attribute
    :param string: the checked string
    :return: whether that string contains a special char out of the string.punctuation string
    """
    for char in string.punctuation:
        if str.__contains__(char):
            return True
    return False

if __name__ == "__main__":
    main()