def stopIterationError ():
    """
    This procedure demonstrats the "stopIterationError" type of error,
    by going over the maximum amount of legal iterations
    :rtype: None
    """
    lst = []
    iterator = iter(lst)
    print(iterator.__next__())
def zeroDivision ():
    """
    This procedure demonstrats the "zeroDivision" type of error,
    by dividing an integer number by zero
    :rtype: None
    """
    print(5 / 2 / 0)
def assertionError ():
    """
    This procedure demonstrats the "assertionError" type of error,
    by applying y so that the assertion for y failes, and is false
    :rtype: None
    """
    y = 0
    assert y != 0, "Invalid Operation"  # denominator can't be 0
    print(5 / y)
def importError():
    """
    This procedure demonstrats the "importError" type of error,
    by trying to import a non-existing module
    :rtype: None
    """
    import hello
def KeyError():
    """
    This procedure demonstrats the "KeyError" type of error,
    by calling a dict with a key that does not exist in it
    :rtype: None
    """
    my_dict = {1:4, 2:5, 3:7}
    print(my_dict[5])
def syntaxError():
    """
    This procedure demonstrats the "syntaxError" type of error,
    by writing an incorrect-syntax statemant, in this case in english instead of python
    :rtype: None
    """
    if this is an error, do something
def IndentationError():
    """
    This procedure demonstrats the "IndentationError" type of error,
    by indentating wrongly, in this case five spaces instead of four
    :rtype: None
    """
     # this isnt indentated correctly
def TypeError():
    """
    This procedure demonstrats the "TypeError" type of error,
    by trying to sum two elements with diffrent types, in this case string and int
    :rtype: None
    """
    print('hello' + 6)


