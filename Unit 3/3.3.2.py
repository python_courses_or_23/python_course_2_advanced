class UnderAge (Exception):
    """
    	A class to represent an UnderAge execption, it extends the general "Exception" class
    	and overides its __init__ and __str__ methods
    	-------
       Methods
       -------
       1) __str__ - this instance method prints the appropriate string for the error, in this case
                    "The given age (age here) is under the age of 18."
    """
    def __init__(self, age):
        """
        This method constructs all the necessary attributes
        for the UnderAge execption object i.e. its age execption
        :param age: the execption's age
        :type age: int
        :rtype: None
        """
        self._age = age
    def __str__(self):
        """
        This instance method prints the appropriate string for the error, in this case
        "The given age (execption's age) is under the age of 18."
        :return: the appropriate string
        """
        return "The given age %d is under the age of 18." % self._age

def send_invitation(name, age):
    """
    This function recieves a name and its age, raised the under age exepction if its under 18
    or prints "You should send an invite to" + that person, otherwise
    :param name: the persons name
    :type name: string
    :param age: the persons age
    :type age: int
    :rtype: None
    """
    if int(age) < 18:
        raise UnderAge(int(age))
    else:
        print("You should send an invite to " + name)

send_invitation("ofek", 20)