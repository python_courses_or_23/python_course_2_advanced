class pixel :
    """
        A class to represent a pixel.
        ----------
        Local attributes
        ----------
        x: the x coordinate of the newly created pixel
        y: the y coordinate of the newly created pixel
        red: the red element (R of the RGB) of the newly created pixel
        green: the greeb element (G of the RGB) of the newly created pixel
        blue: the blue element (B of the RGB) of the newly created pixel
        -------
        Methods
        -------
        1) set_coords - this instance method recives a new x and y coordinats and updates them
        2) set_grayscale - this instance method sets all R, G and B valus to the avarage of the current red, green and blue
        3) print_pixel_info - this instance method prints the current state of the pixel in the format of
        X : _, Y : _, Color : R, G, B, color which is set to zero
    """
    def __init__(self, x=0, y=0, red=0, green=0, blue =0):
        """
        This method constructs all the necessary attributes
        for the pixel object i.e. the x, y, red, green and blue
        :param x: the x coordinate of the newly created pixel
        :type x: int
        :param y: the y coordinate of the newly created pixel
        :type y: int
        :param red: the red element (R of the RGB) of the newly created pixel
        :type red: int
        :param green: the greeb element (G of the RGB) of the newly created pixel
        :type green: int
        :param blue: the blue element (B of the RGB) of the newly created pixel
        :type blue: int
        :rtype: None
        """
        self.x = x
        self.y = y
        self.red = red
        self.green = green
        self.blue = blue

    def set_coords(self, x, y):
        """
        This instance method recives a new x and y coordinats
        and updates them
        :param x: the new x coord
        :type x: int
        :param y: the new y coord
        :type y: int
        :rtype: None
        """
        self.x = x
        self.y = y

    def set_grayscale(self):
        """
        This instance method sets all R, G and B valus
        to the avarage of the current red, green and blue
        :rtype: None
        """
        avg = int((self.red + self.green + self.blue) / 3)
        self.red = avg
        self.green = avg
        self.blue = avg

    def print_pixel_info(self):
        """
        This instance method prints the current state of the pixel in the format of
        X : _, Y : _, Color : R, G, B, color which is set to zero
        :rtype: None
        """
        if self.red == 0 and self.green == 0 and self.blue !=0 and self.blue > 50:
            color = "Blue"
            zero = True
        elif self.red == 0 and self.green != 0 and self.blue ==0 and self.green > 50:
            color = "Green"
            zero = True
        elif self.red != 0 and self.green == 0 and self.blue ==0 and self.red > 50:
            color = "Red"
            zero = True
        else :
            zero = False
        if zero == True :
            print("X : ", self.x , "," , "Y : ", self.y , ",", "Color : ", (self.red, self.green, self.blue), color)
        else :
            print("X : ", self.x , "," , "Y : ", self.y , ",", "Color : ", (self.red, self.green, self.blue))

my_pixel = pixel(5,6,250,0,0)
my_pixel.print_pixel_info()
my_pixel.set_grayscale()
my_pixel.print_pixel_info()



