class Lion :
    """
    A class to represent a lion.
    ----------
    Attributes
    ----------
    name - the name of the lion
    age - the age of that lion
    -------
    Methods
    -------
    1) birthday - an instance method which acts as a birthday to that lion,
       effectivly adding an additional year to the animal's age
    2) get_age - this instance method returns the age of the given lion (self)
    """

    def __init__(self):
        """
        This method constructs all the necessary attributes
        for the lion object i.e. the name as default - "Lion", and
        the age set to be 0 at start
        :rtype: None
        """
        self.name = "Lion"
        self.age = 0
    def birthday (self):
        """
        This method is an instance method which acts as a birthday to that lion,
        effectivly adding an additional year to the animal's age
        :rtype: None
        """
        self.age += 1
    def get_age (self):
        """
        This instance method returns the age of the lion
        :return: the animals current age
        :rtype: int
        """
        return self.age

lion1 = Lion()
lion2 = Lion()
lion1.birthday()
print(lion1.get_age())
print(lion2.get_age())

