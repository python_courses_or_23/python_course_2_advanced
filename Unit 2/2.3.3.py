class Lion :
    """
    A class to represent a lion.
    ----------
    Local attributes
    ----------
    name - the name of the lion
    age - the age of that lion
    ----------
    Global attributes
    ----------
    count_animals - the quanitty of created lions
    -------
    Methods
    -------
    1) birthday - an instance method which acts as a birthday to that lion,
       effectivly adding an additional year to the animal's age
    2) get_age - this instance method returns the age of the given lion (self)
    3) set_name - this instance method recieves a new name for the lion and changes it
    4) get_name - this instance method returns the name of the given lion (self)
    """
    count_animals = 0
    def __init__(self, name = "Lionel"):
        """
        This method constructs all the necessary attributes
        for the lion object i.e.the name and
        the age set to be 0 at start.
        Also, that method increments the "count_animals" global variable by one, since
        there has been a creation of a Lion object
        :param name: the given name of the lion, the default is the name "Lionel"
        :type name: string
        :rtype: None
        """
        self._name = name
        self._age = 0
        Lion.count_animals += 1
    def birthday (self):
        """
        This method is an instance method which acts as a birthday to that lion,
        effectivly adding an additional year to the animal's age
        :rtype: None
        """
        self._age += 1
    def get_age (self):
        """
        This instance method returns the age of the lion
        :return: the animals current age
        :rtype: int
        """
        return self._age
    def set_name(self, new_name):
        """
        This method recieves a new name for the lion and changes it
        :param new_name: the new name of the lion
        :type new_name: string
        :rtype: None
        """
        self._name = new_name
    def get_name(self):
        """
        This method returns the current name of the lion
        :return: The name of the current lion
        :rtype: string
        """
        return self._name


lion1 = Lion("Test lion")
lion2 = Lion()

print(lion1.get_name())
print(lion2.get_name())

lion2.set_name("Changed name")
print(lion2.get_name())

print(Lion.count_animals)


