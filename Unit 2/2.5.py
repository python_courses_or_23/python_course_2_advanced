class Animal :
    """
        A class to represent an Animal.
       ----------
       Local attributes
       ----------
       _name: the animal's name
       _hunger: the animal's hunger level (an integer number)
       ----------
       Global attributes
       ----------
       zoo_name: the zoo's name, in this case "Hayaton"
       -------
       Methods
       -------
       1) get_name - this instance method returns the name of the animal
       2) is_hungry - this instance method returns whether this animal is hungery, meaning does it have a hunger level greater than  zero
       3) feed - this instance method represents a feeding method to the animal i.e. it decrements the its level
       4) talk - this instance method is implemented inside every extending class to be used as a talk method
    """
    zoo_name = "Hayaton"
    def __init__(self, name, hunger = 0):
        """
        This method constructs all the necessary attributes
        for the Animal object i.e. the its name and its hunger levek, whilst the hunger level is
        automatically set to zero
        :param _name: the animal's name
        :type _name: string
        :type _hunger: the animal's hunger level (an integer number)
        :type: _hunger: int
        :rtype: None
        """
        self._name = name
        self._hunger = hunger
    def get_name (self):
        return self._name
    def is_hungry (self):
        return self._hunger > 0
    def feed (self):
        self._hunger -= 1
    def talk(self):
        pass

class Dog (Animal) :
    """
        A class to represent a Dog, it extends the general "Animal" class
        and implements the talk method as well as the unique method - fetch stick
        -------
       Methods
       -------
       1) talk - this instance method acts as talk method for the dog
       2) fetch_stick - this instance method prints "There you go, sir!" after the dog had fetched the stick
    """
    def talk(self):
        """
        This instance method acts as talk method for the dog
        :rtype: None
        """
        print("woof woof")
    def fetch_stick(self):
        """
        This instance method prints "There you go, sir!" after the dog had fetched the stick
        :rtype: None
        """
        print("There you go, sir!")

class Cat (Animal) :
    """
        A class to represent a Cat, it extends the general "Animal" class
        and implements the talk method as well as the unique method - chase laser
        -------
       Methods
       -------
       1) talk - this instance method acts as talk method for the cat
       2) chase_laser - this instance method prints "Meeeeow" after the cat had chased the laser
    """
    def talk(self):
        """
        This instance method acts as talk method for the cat
        :rtype: None
        """
        print("meow")
    def chase_laser(self):
        """
        This instance method prints "Meeeeow" after the cat had chased the laser
        :rtype: None
        """
        print("Meeeeow")

class Skunk (Animal):
    """
    	A class to represent a Skunk, it extends the general "Animal" class
    	and implements the talk method as well as the unique method - stink
    	-------
       Methods
       -------
       1) talk - this instance method acts as talk method for the skunk
       2) stink - this instance method prints "Dear lord!" after the unique method of the skunk
    """

    def __init__(self, name, _stink_count = 6):
        """
       This method constructs all the necessary attributes
       for the Skunk object i.e. its name and its stick count, whilst the stick count is
       automatically set to six. Firstly, it calls the super __init__ method i.e. the Animal class
       initializer, then adds the stick count
       :param _name: the skunk's name
       :type _name: string
       :type _stink_count: the animal's stick count (an integer number)
       :type: _stink_count: int
       :rtype: None
       """
        Animal.__init__(self, name)
        self._stink_count = _stink_count
    def talk(self):
        """
        This instance method acts as talk method for the skunk
        :rtype: None
        """
        print("tsssss")
    def stink(self):
        """
        This instance method prints "Dear lord!" after the unique method of the skunk
        :rtype: None
        """
        print("Dear lord!")

class Unicorn (Animal):
    """
    	A class to represent a Unicorn, it extends the general "Animal" class
    	and implements the talk method as well as the unique method - sing
    	-------
       Methods
       -------
       1) talk - this instance method acts as talk method for the unicorn
       2) chase_laser - this instance method prints "I’m not your toy..." after the unicorn had sang
    """
    def talk(self):
        """
        This instance method acts as talk method for the unicorn
        :rtype: None
        """
        print("Good day, darling")
    def sing(self):
        """
        This instance method prints "I’m not your toy..." after the unicorn had sang
        :rtype: None
        """
        print("I’m not your toy...")

class Dragon (Animal):
    """
    	A class to represent a Dragon, it extends the general "Animal" class
    	and implements the talk method as well as the unique method - breath fire
    	-------
       Methods
       -------
       1) talk - this instance method acts as talk method for the dragon
       2) chase_laser - this instance method prints "$@#$#@$" after the dragon had breathed fire
    """
    def __init__(self, name, _color = "Green"):
        """
       This method constructs all the necessary attributes
       for the Dragon object i.e. the its name and its stick color, whilst the color is
       automatically set to be green ("Green"). Firstly, it calls the super __init__ method i.e. the Animal class
       initializer, then adds the color
       :param _name: the dragon's name
       :type _name: string
       :type _color: the dragon's color
       :type: _color: string
       :rtype: None
       """
        Animal.__init__(self, name)
        self._color = _color
    def talk(self):
        """
        This instance method acts as talk method for the dragon
        :rtype: None
        """
        print("Raaaawr")
    def breath_fire(self):
        """
        This instance method prints "$@#$#@$" after the dragon had breathed fire
        :rtype: None
        """
        print("$@#$#@$")

def main():
    a1 = Dog("Bronwie", 10)
    a2 = Cat("Zelda", 3)
    a3 = Skunk("Stinky")
    a4 = Unicorn("Keith", 7)
    a5 = Dragon("Lizzy", 1450)
    a6 = Dog("Doggo", 80)
    a7 = Cat("Kitty", 80)
    a8 = Skunk("Stinky Jr.", 80)
    a9 = Unicorn("Clair", 80)
    a10 = Dragon("McFly", 80)

    lst = list([a1, a2, a3, a4, a5, a6, a7, a8, a9, a10])

    for animal in lst:
        subclass = ""
        if isinstance(animal, Dog) :
            subclass = "Dog"
        elif isinstance(animal, Cat) :
            subclass = "Cat"
        elif isinstance(animal, Skunk):
            subclass = "Skunk"
        elif isinstance(animal, Unicorn):
            subclass = "Unicorn"
        else:
            subclass = "Dragon"

        if animal.is_hungry():
            print(subclass, animal.get_name())
        while animal.is_hungry():
            animal.feed()
        animal.talk()
        if subclass == "Dog" :
            animal.fetch_stick()
        elif subclass == "Cat":
            animal.chase_laser()
        elif subclass == "Skunk":
            animal.stink()
        elif subclass == "Unicorn":
            animal.sing()
        else :
            animal.breath_fire()
    print(Animal.zoo_name)

if __name__ == "__main__":
    main()

