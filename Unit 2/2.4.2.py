import math
class BigThing :
    """
        A class to represent a Big Thing.
       ----------
       Local attributes
       ----------
       thing: the things itself
       -------
       Methods
       -------
       1) size - this instance method returns the thing if the thing is
          an int type of object, else, if its a list, and dict or a string
          it returns the length of the thing.
    """
    def __init__(self, the_thing):
        """
        This method constructs all the necessary attributes
        for the BigThing object i.e. the thing
        :param the_thing: the things itself
        :type the_thing: UNKNOWN / BigThing
        :rtype: None
        """
        self.thing = the_thing
    def size (self):
        """
        his instance method returns the thing if the thing is
        an int type of object, else, if its a list, and dict or a string
        it returns the length of the thing.
        :return: the thing if the thing is an int,
                 and its length if it's a list, dict or string
        """
        if isinstance(self.thing, int) :
            return self.thing
        elif isinstance(self.thing, (list, dict, str)) :
            return len(self.thing)

class BigCat (BigThing) :
    """
        A class to represent a big Cat.
       ----------
       Local attributes
       ----------
       thing: the things itself, in this case, a CAT
       weight: the weight of the big cat
       -------
       Methods
       -------
       1) size - this instance method returns the string "Fat" if the weight
                 is more than 15 and less or equal to 20, returns "Very fat" if
                 it's above 20 and "OK" otherwise
    """
    def __init__(self, the_thing, weight):
        """
        This method constructs all the necessary attributes
        for the BigThing object i.e. the thing and its weight
        :param the_thing: the big cat object
        :type the_thing: BigCat
        :param weight: the weight of the big cat
        :type weight: int
        :rtype: None
        """
        BigThing.__init__(self, the_thing)
        self.weight = weight
    def size(self):
        """
        This instance method returns the thing if the thing is
        an int type of object, else, if its a list, and dict or a string
        it returns the length of the thing.
        :return: "Fat" if 15 < weight <= 20
                 "Very fat" if weight > 20
                 "Ok" otherwise
        """
        if self.weight > 15 and self.weight <= 20:
            return "Fat"
        elif self.weight > 20:
            return "Very fat"
        else :
            return "Ok"

my_thing = BigThing("balloon")
print(my_thing.size())

cutie = BigCat("mitzy", 22)
print(cutie.size())