from gtts import gTTS
# This module is imported so that we can
# play the converted audio
import os

txt = "first time i'm using a package in next.py course" # setting the text
lang = "en" # setting the langugue --> en for english 
myobj = gTTS(text=txt, lang=lang, slow=False) # creating the GTTS object from the text, in the correct langugue

myobj.save("first.mp3") # saving the object as a mp3 object (audio object --> speech)

# Playing the converted file
os.system("start first.mp3") # playing the mp3 file using the operating system import, using "start"