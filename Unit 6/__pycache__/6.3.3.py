from gtts import gTTS
# This module is imported so that we can
# play the converted audio
import os

txt = "first time i'm using a package in next.py course"
lang = "en"
myobj = gTTS(text=txt, lang=lang, slow=False)

myobj.save("first.mp3")

# Playing the converted file
os.system("start first.mp3")