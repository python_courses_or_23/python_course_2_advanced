from file1 import GreetingCard

class BirthdayCard (GreetingCard) :
   def __init__(self, rec, send, senderAge = 0):
       GreetingCard.__init__(self, rec, send)
       self._senderAge = senderAge
   def greeting_msg (self):
       print("The recipient of the card is %s and its sender is %s. Happy birthday ! your age is %d" % (self._recipient, self._sender, self._senderAge))
