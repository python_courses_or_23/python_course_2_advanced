import tkinter as tk

window = tk.Tk() # creating the window from the tkinter module
window.geometry("1500x1500") # defining its height and width

# Open the image file
image_file = 'pic2.png' # the source for the image that will be displayed

def handle_click(event): # handling the clicks to show the image of George Washington
    canvas = tk.Canvas( # the canvas to contain the image
        window,
        width=1500,
        height=1500
    )
    img = tk.PhotoImage(file=image_file) # creating the image
    canvas.image = img  # store a reference to the PhotoImage object
    canvas.create_image(
        10,
        10,
        anchor=tk.NW,
        image=img
    )
    canvas.pack() # adding the image to the window created earlier

greeting = tk.Label( # label with the text of the question
    text="Who was the first president of the United States of America?",
    foreground="green",
    width=50,
    height=10
)

button = tk.Button( # the clicking button
    text="Click to find out!",
    width=25,
    height=5,
    bg="blue",
    fg="yellow",
)
button.bind("<Button-1>", handle_click) # binding the mouse left click, ON THIS BUTTON, to the clicking function presented before

greeting.pack() # adding the text label to the window created earlier
button.pack() # adding the button to the window created earlier

window.mainloop() # the thread that runs the whole window
